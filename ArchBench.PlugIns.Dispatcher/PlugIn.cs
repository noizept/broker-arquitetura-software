﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Runtime.Remoting.Contexts;
using System.Security.Policy;
using System.Text;
using System.Threading;
using HttpServer;
using HttpServer.Sessions;
using System.Net.Sockets;


namespace ArchBench.PlugIns.Dispatcher
{
    public class IpPor
    {
        public int Port { get; set; }
        public string Ipnumber { get; set; }

        public IpPor(int aPort, string aIp)
        {
            Port = aPort;
            Ipnumber = aIp;
        }

    }
    public class PlugIn : IArchServerModulePlugIn
    {
        private TcpListener mListener;
        private Thread      mRegisterThread;
        private Thread mTeste;

        private  readonly IDictionary<string,IpPor> mServers = new Dictionary<string, IpPor>(); 

        public PlugIn()
        {

            mListener = new TcpListener(  IPAddress.Any, 9000 );
        }



        private void Unregist(string aName)
        {
            mServers.Remove(aName);
            Host.Logger.WriteLine("Removed Server Timeout:" + aName);

        }


        private void ReceiveThreadFunction()
        {
            try
            {
                // Start listening for client requests.
                mListener.Start();

                // Buffer for reading data
                Byte[] bytes = new Byte[256];

                // Enter the listening loop.
                while (true)
                {
                    // Perform a blocking call to accept requests.
                    // You could also user server.AcceptSocket() here.
                    TcpClient client = mListener.AcceptTcpClient();

                    // Get a stream object for reading and writing
                    NetworkStream stream = client.GetStream();

                    int count = stream.Read(bytes, 0, bytes.Length);
                    if (count != 0)
                    {
                        // Translate data bytes to a ASCII string.
                        String data = Encoding.ASCII.GetString( bytes, 0, count );
                        // Using the RemoteEndPoint property.
                        

                        // Ip do servidor a se conectar
                        String serverIP =client.Client.RemoteEndPoint.ToString();
                        serverIP = serverIP.Substring(0, serverIP.IndexOf(':'));
                                     
                        // nome do servidor e Porta
                        String server = data.Substring( 0, data.IndexOf('-') );
                        String port   = data.Substring( data.IndexOf('-') + 1 );
                       
                        // Convertido para int pk o TCP client precisa String->ip e Int->port
                        int portToint = Int32.Parse(port);
                        Host.Logger.WriteLine( String.Format( "Server {0} available on port {1}", server, port ) );
                        
                        //Ip e porta do servidor que vai registar
                        IpPor currentServer = new IpPor(portToint, serverIP);
                        Regist( server,currentServer);
                    }

                    client.Close();
                }
            }
            catch ( SocketException e )
            {
                Host.Logger.WriteLine( String.Format( "SocketException: {0}", e ) );
            }
            finally
            {
               mListener.Stop();
            }
        }

        private void Regist( string aServer, IpPor aIpPor )
        {
            if ( mServers.ContainsKey(aServer) ) return;
            mServers.Add( aServer,aIpPor );
            
        }   


        #region IArchServerModulePlugIn Members

        public bool Process( IHttpRequest aRequest, IHttpResponse aResponse, IHttpSession aSession )
        {
           // mNextServer = ++mNextServer % mServers.Count;

            var redirection = new StringBuilder();

            //Server Nome + Path 
            var urlPath = aRequest.Uri.AbsolutePath.Substring(1);
            var serverName = urlPath.Substring(0, urlPath.IndexOf('/'));
            urlPath = urlPath.Substring(urlPath.IndexOf('/'));

            if (mServers.ContainsKey(serverName))
            {
                redirection.AppendFormat("http://{0}:{1}", mServers[serverName].Ipnumber, mServers[serverName].Port);
                redirection.AppendFormat(urlPath);
                aResponse.Redirect(redirection.ToString()); 

           }


            return true;
        }

        #endregion

        #region IArchServerPlugIn Members

        public string Name
        {
            get { return "ArchServer Dispatcher Plugin"; }
        }

        public string Description
        {
            get { return "Dispatch clients to the proper server"; }
        }

        public string Author
        {
            get { return "Leonel Nobrega"; }
        }

        public string Version
        {
            get { return "1.0"; }
        }

        public IArchServerPlugInHost Host
        {
            get; set;
        }

        public void Initialize()
        {
            mRegisterThread = new Thread( ReceiveThreadFunction ) {IsBackground = true};
            mRegisterThread.Start();
            mTeste=new Thread(Runner) {IsBackground = true};
            mTeste.Start();

        }
        #endregion
        private void Runner(object obj)
        {
            while (true)
            {
                var t = new Thread(Heartbeat);
                t.Start();
                Thread.Sleep(10000);
            }
        }

 
        public void Heartbeat()
        {
            if (mServers.Count == 0) return;
            var testCopy = new Dictionary<string,IpPor>(mServers);
            TcpClient mTestcon = null;
            var aRemover= new List<string>();
            foreach (var port in testCopy)
            {
                try
                {

                    mTestcon = new TcpClient(port.Value.Ipnumber,port.Value.Port);

                }
                catch (Exception)
                {
                    aRemover.Add(port.Key);
                } 
            }
            if (mTestcon != null) mTestcon.Close();

            foreach (var tirafora in aRemover)
            {
                Unregist(tirafora);

            }
        }
        

        public void Dispose()
        {
        }

      
    }
}
