﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using ArchBench;
using HttpServer;
using HttpServer.Sessions;


namespace BrokerV2
{
    public class BrokerV2 : IArchServerModulePlugIn
    {
        private int mNext = 0;
        private TcpListener mListener;
        private Thread mRegisterThread;
        private IDictionary<string,List<string>> _mServers = new Dictionary<string, List<string>>();
       
        public bool Process(IHttpRequest aRequest, IHttpResponse aResponse, IHttpSession aSession)
        {


               var serviceName = GetService(aRequest);
                if (!ServiceExist(serviceName))
                {
                    Host.Logger.WriteLine("WRONG SERVICE NAME");
                    return true;
                }

            var serverToService =GetCurrentServer(aRequest, aResponse, serviceName);
            serverToService = Uri.UnescapeDataString(serverToService);
                
            var UrlPath = urlPath(aRequest);


                WebClient myWebclient = new WebClient();
                CookieToWebclient(aRequest,myWebclient);
                
            byte[] content;


            if (aRequest.Method == Method.Get)
            {
                content = myWebclient.DownloadData(serverToService + UrlPath);

            }
            else if (aRequest.Method == Method.Post)
            {
                var formInputs = new NameValueCollection();
                foreach (HttpInputItem item in aRequest.Form)
                {
                    Host.Logger.WriteLine("==> [{0}] := {1}", item.Name, item.Value);
                    formInputs[item.Name] = item.Value;
                }
               content = myWebclient.UploadValues(serverToService + UrlPath, "POST", formInputs);
            }
            else
            {
                content = myWebclient.DownloadData(serverToService + UrlPath);
;
            }
            StreamWriter writer = new StreamWriter(aResponse.Body);
            writer = ProcessHTML(myWebclient, writer, content,aResponse, serviceName, serverToService);
        //    content = HTMLParsing(content, serviceName, serverToService);
            
            writer.Flush();
            CookieToResponse(aResponse, myWebclient);
            aResponse.Send();
            myWebclient.Dispose();


            return true;
        }
        private StreamWriter ProcessHTML(WebClient client, StreamWriter writer, byte[] bytes, IHttpResponse aResponse, string aServiceName, string aServer)
        {
            string html = null;
            
            if (client.ResponseHeaders["Content-Type"] == "text/html" || client.ResponseHeaders["Content-Type"].Substring(0, client.ResponseHeaders["Content-Type"].IndexOf("/")) == "text")
            {
                html = Encoding.ASCII.GetString(bytes, 0, bytes.Length);
                html = HTMLParsing(html, aServiceName, aServer);
                writer.WriteLine(html);
            }
            else
            {
                Host.Logger.WriteLine("Content type probably image: " + client.ResponseHeaders["Content-Type"]);
                aResponse.AddHeader("Content-Type", client.ResponseHeaders["Content-Type"]);
                writer.BaseStream.Write(bytes, 0, bytes.Length);
            }
            return writer;
        }


        #region Cookies e Serviço
        private static String[] mTags = { "href", "src", "url", "action" };
        private string HTMLParsing(string aHTML, string aService,string aServerIP)
        {

            foreach (var tag in mTags)
            {



                String orig;
                String modded;

                orig = tag + "=\"/";
                modded = tag + "=\"/" + aService + "/";
                aHTML = aHTML.Replace(orig, modded);
               

            }

            /*
            var regex = new Regex(@"<(?<Tag>((a)|img|form))\b[^>]*?\b(?<Type>(?(1)(href|src|action)))\s*=\s*(?:""(?<URL>(?:\\""|[^""])*)""|'(?<URL>(?:\\'|[^'])*)')", RegexOptions.IgnoreCase); 
            Match match;
            for (match = regex.Match(aHTML); match.Success; match = match.NextMatch())
            {
                if (match.Groups["URL"].Value.StartsWith(aServerIP))
                   aHTML = aHTML.Replace(aServerIP, "/"+aService);

                if (match.Groups["URL"].Value.StartsWith("/"))
                {
                    //if(match.Groups["Tag"].Value=="img")
                      //      aHTML = aHTML.Replace(match.Groups["URL"].Value, aServerIP+ match.Groups["URL"].Value);
                       // else
                            aHTML = aHTML.Replace(match.Groups["URL"].Value, "/" + aService + match.Groups["URL"].Value);
                }

            }*/

            return aHTML;
        }
        private string urlPath(IHttpRequest aRequest)
        {
            try
            {
                string path = string.Copy(aRequest.UriPath);
                path = path.Substring(1);
                return path.Substring(path.IndexOf("/", System.StringComparison.Ordinal));
            }
            catch (Exception)
            {

                return "";
            }

        }

        private bool ServiceExist(string serviceName)
        {
            return _mServers.ContainsKey(serviceName);
        }

        private string GetService(IHttpRequest aRequest)
        {
            try
            {
                var service = string.Copy(aRequest.UriPath.Substring(1));
                var xx = service.Substring(0, service.IndexOf("/"));
                return xx;
            }
            catch (Exception)
            {
                return "!%$&$%/%&/%&/%(/(/&(&(";
            }
        }


        private static string GetCookie(IHttpRequest aHttpRequest, string cookieName)
        {
            foreach (RequestCookie requestCookies in aHttpRequest.Cookies)
            {
                if (requestCookies != null && requestCookies.Name == cookieName)
                    return requestCookies.Value;
            }
            return null;
        }

        void CookieToWebclient(IHttpRequest aRequest, WebClient aClient)
        {
            if (string.IsNullOrEmpty(aRequest.Headers["Cookie"])) return;
            foreach (RequestCookie cookie in aRequest.Cookies)
            {
                aClient.Headers.Add("Cookie", aRequest.Headers["Cookie"]);
            }
        }

        void CookieToResponse(IHttpResponse aResponse, WebClient aClient)
        {
            if (string.IsNullOrEmpty(aClient.ResponseHeaders["Set-Cookie"])) return;
            aResponse.AddHeader("Set-Cookie", aClient.ResponseHeaders["Set-Cookie"]);
        }

        private string GetCurrentServer(IHttpRequest aRequest, IHttpResponse aResponse, string aService)
        {
            // Verifica se existe cookie de servidor que é criada a primeira vez que ele liga
            // Se ñ houver cria uma cookie com o ID do servidor que ficou ligado
            // Caso contrario buscar o index do servidor que já tá bind
            string cookie = GetCookie(aRequest, aService);

            if (!string.IsNullOrEmpty(cookie)) return cookie;
            mNext = ++mNext % _mServers[aService].Count;
            string server = _mServers[aService][mNext];
            var mCookie = new ResponseCookie(aService, server, DateTime.Now.AddDays(1d));
            aResponse.Cookies.Add(mCookie);
            return server;
        }
#endregion
        #region Members

        public BrokerV2()
        {
            mListener = new TcpListener(IPAddress.Any, 9000);
        }
        public string Name { get { return "Broker V2"; } }
        public string Description { get { return "Broker V2"; } }
        public string Author { get { return "Sérgio"; } }
        public string Version { get { return "2.0"; } }
        public IArchServerPlugInHost Host { get; set; }
        public void Initialize()
        {
            mRegisterThread = new Thread(ReceiveThreadFunction) { IsBackground = true };
            mRegisterThread.Start();
        }


        public static string GetLocalIP()
        {
            // Resolves a host name or IP address to an IPHostEntry instance.
            // IPHostEntry - Provides a container class for Internet host address information. 
            IPHostEntry IPHostEntry = Dns.GetHostEntry(Dns.GetHostName());

            // IPAddress class contains the address of a computer on an IP network. 
            foreach (IPAddress IPAddress in IPHostEntry.AddressList)
            {
                // InterNetwork indicates that an IP version 4 address is expected 
                // when a Socket connects to an endpoint
                if (IPAddress.AddressFamily.ToString() != "InterNetwork") continue;
                return IPAddress.ToString();
            }
            return @"127.0.0.1";
        }
       
        
        private void ReceiveThreadFunction()
        {
            try
            {
                // Start listening for client requests.
                mListener.Start();

                // Buffer for reading data
                Byte[] bytes = new Byte[256];

                // Enter the listening loop.
                while (true)
                {
                    // Perform a blocking call to accept requests.
                    // You could also user server.AcceptSocket() here.
                    TcpClient client = mListener.AcceptTcpClient();

                    // Get a stream object for reading and writing
                    NetworkStream stream = client.GetStream();

                    int count = stream.Read(bytes, 0, bytes.Length);
                    if (count != 0)
                    {
                        // Translate data bytes to a ASCII string.
                        String data = Encoding.ASCII.GetString(bytes, 0, count);

                        string server = data.Substring(0, data.IndexOf("@"));

                        string serverIP = data.Substring(data.IndexOf("@")+1);
                        serverIP = serverIP.Substring(0, serverIP.IndexOf(":"));
                        
                        string port = data.Substring(data.IndexOf(":") + 1);

                        Regist(server,serverIP,port);
                        Host.Logger.WriteLine(String.Format("Server {0} available on port {1}", server, port));
                        

                    }

                    client.Close();
                }
            }
            catch (SocketException e)
            {
                Host.Logger.WriteLine(String.Format("SocketException: {0}", e));
            }
            finally
            {
                mListener.Stop();
            }
        }

        /// <summary>
        /// Adiciona o tupplo com NomeServiço-IpdoServiço-PortadoServiço
        /// </summary>
        /// <param name="aServername"></param>
        /// <param name="aServerip"></param>
        /// <param name="aServerPort"></param>
        private void Regist(string aServername,  string aServerip,string aServerPort)
        {


            if (!_mServers.ContainsKey(aServername))
            {
                _mServers.Add(aServername,new List<string>());
            }
            var url = "http://" + aServerip + ":" + aServerPort;
            _mServers[aServername].Add(url);
            _mServers[aServername] = _mServers[aServername].Distinct().ToList();
        }

        public void Dispose()
        {
        }
        #endregion
    }
}
