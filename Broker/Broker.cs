﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Web;
using HttpServer;
using HttpServer.Sessions;


namespace ArchBench.Plugins.Broker
{

    public class IpPor
    {
        public int Port { get; set; }
        public string Ipnumber { get; set; }

        public IpPor(int aPort, string aIp)
        {
            Port = aPort;
            Ipnumber = aIp;

        }

    }
    public class Broker : IArchServerModulePlugIn
    {

        private readonly IDictionary<string,IpPor> mServers = new Dictionary<string, IpPor>();

        private TcpListener mListener;
        private Thread mRegisterThread;
        private int mNextServer = -1;

        
        /// <summary>
        /// Apanha o Path do tipo /XX/XX/XX sem a IP:porta do servidor
        /// </summary>
        /// <param name="aRequest"></param>
        /// <returns></returns>
        private String GetUrlPath(IHttpRequest aRequest)
        {
            return aRequest.UriPath;

        }
        /// <summary>
        /// vê se existem cookies com o Request devolve true or false
        /// </summary>
        /// <param name="aRequest"></param>
        /// <returns></returns>
        private bool HasCookie(IHttpRequest aRequest)
        {
            return aRequest.Cookies.Count > 0;
        }
        /// <summary>
        /// Buscar o IP do servidor:porta retorna em string
        /// </summary>
        /// <param name="aIndex"></param>
        /// <returns></returns>
        string GetIp(int aIndex)
        {
            try
            {

                var serverName = mServers.Keys.ElementAt(aIndex);
                return "http://" + mServers[serverName].Ipnumber + ":" + mServers[serverName].Port;
            }
            catch (Exception)
            {
                
                Host.Logger.WriteLine("WRONG URL");
            }
            return null;
        }

        private string GetCurrentServer(IHttpRequest aRequest, IHttpResponse aResponse, string currentServer)
        {
            // Verifica se existe cookie de servidor que é criada a primeira vez que ele liga
            // Se ñ houver cria uma cookie com o ID do servidor que ficou ligado
            // Caso contrario buscar o index do servidor que já tá bind
            if (!HasCookie(aRequest))
            {
                mNextServer = ++mNextServer % mServers.Count;
                currentServer = GetIp(mNextServer);
                var mCookie = new ResponseCookie("gotServer", mNextServer.ToString(), DateTime.Now.AddDays(1d));
                aResponse.Cookies.Add(mCookie);
            }
            else
                currentServer = GetIp(Int32.Parse(aRequest.Cookies["gotServer"].Value));
            return currentServer;
        }


        void CookieToWebclient(IHttpRequest aRequest,WebClient aClient)
        {
         if(string.IsNullOrEmpty(aRequest.Headers["Cookie"]))  return;
            foreach (RequestCookie cookie in aRequest.Cookies)
            {
                aClient.Headers.Add("Cookie",aRequest.Headers["Cookie"]);
            }
        }

        void CookieToResponse(IHttpResponse aResponse, WebClient aClient)
        {
            if (string.IsNullOrEmpty(aClient.ResponseHeaders["Set-Cookie"])) return;
            aResponse.AddHeader("Set-Cookie", aClient.ResponseHeaders["Set-Cookie"]);
        }

        

        public bool Process(IHttpRequest aRequest, IHttpResponse aResponse, IHttpSession aSession)
        {
            string currentServer = null;

            //Verifica se existe Cookie se nao houver ja cria para prender o servidor e trás o IP
            currentServer = GetCurrentServer(aRequest, aResponse, currentServer);
            if (string.IsNullOrEmpty(currentServer)) return true;

            WebClient myWebclient = new WebClient();
            CookieToWebclient(aRequest,myWebclient);
            
            currentServer += GetUrlPath(aRequest);
            byte[] content;
                
               //HTTP @GET
                if (aRequest.Method == Method.Get)
                {

                    // AAMZON ACRESCENTAR O URL ANTES 
                    // CASO O PATH TEJA ERRADO APANHAR EXCEPÇAO
                    try
                    {
                         content = myWebclient.DownloadData(currentServer);
                    }
                    catch (Exception)
                    {
                        
                        Host.Logger.WriteLine("WRONG URL");
                        return true;
                    }
                   StreamWriter writer = new StreamWriter(aResponse.Body); 
                   aResponse.ContentType = myWebclient.ResponseHeaders["Content-Type"];
                   CookieToResponse(aResponse,myWebclient);

                   if (myWebclient.ResponseHeaders[3] != "text/html" || myWebclient.ResponseHeaders["Content-Type"].Substring(0, myWebclient.ResponseHeaders["Content-Type"].IndexOf(";", StringComparison.Ordinal)) != "text/html")                           
                       writer.BaseStream.Write(content,0,content.Length);
                   else
                       writer.WriteLine(Encoding.ASCII.GetString(content));

                   
                   writer.Flush();
                   aResponse.Send();
                   writer.Close();

                   myWebclient.Dispose();
                 
                }

                //HTTP @POST
                else if (aRequest.Method == Method.Post)
                {

                    var formInputs= new NameValueCollection();
                    foreach ( HttpInputItem item in aRequest.Form )
                    {
                        Host.Logger.WriteLine( "==> [{0}] := {1}", item.Name, item.Value );
                        formInputs[item.Name] = item.Value;
                    }

                    myWebclient.UploadValues(currentServer, "POST", formInputs);

                    CookieToResponse(aResponse, myWebclient);
                    aResponse.Send();
                    myWebclient.Dispose();

                }

                       

            return true;
        }


        #region Membros

        public Broker()
        {
            mListener = new TcpListener(IPAddress.Any, 9000);

        }
        public string Name
        {
            get
            {
                return "~Plugin Broker";
            }
        }

        public string Description
        {
            get
            {
                return "Broker tryout";
            }
        }

        public string Author
        {
            get { return "Sérgio"; }
        }

        public string Version
        {
            get
            {
                return "1.0";
                
            }
        }

        public IArchServerPlugInHost Host { get; set; }
        public void Initialize()
        {
            mRegisterThread = new Thread(ReceiveThreadFunction) { IsBackground = true };
            mRegisterThread.Start();
        }

        public void Dispose()
        {
        }
        private void ReceiveThreadFunction()
        {
            try
            {
                // Start listening for client requests.
                mListener.Start();

                // Buffer for reading data
                Byte[] bytes = new Byte[256];

                // Enter the listening loop.
                while (true)
                {
                    // Perform a blocking call to accept requests.
                    // You could also user server.AcceptSocket() here.
                    TcpClient client = mListener.AcceptTcpClient();

                    // Get a stream object for reading and writing
                    NetworkStream stream = client.GetStream();

                    int count = stream.Read(bytes, 0, bytes.Length);
                    if (count != 0)
                    {
                        // Translate data bytes to a ASCII string.
                        String data = Encoding.ASCII.GetString(bytes, 0, count);
                        // Using the RemoteEndPoint property.


                        // Ip do servidor a se conectar
                        String serverIP = client.Client.RemoteEndPoint.ToString();
                        serverIP = serverIP.Substring(0, serverIP.IndexOf(':'));

                        // nome do servidor e Porta
                        String server = data.Substring(0, data.IndexOf('-'));
                        String port = data.Substring(data.IndexOf('-') + 1);

                        // Convertido para int pk o TCP client precisa String->ip e Int->port
                        int portToint = Int32.Parse(port);
                        Host.Logger.WriteLine(String.Format("Server {0} available on port {1}", server, port));

                        //Ip e porta do servidor que vai registar
                        IpPor currentServer = new IpPor(portToint, serverIP);
                        Regist(server, currentServer);
                    }

                    client.Close();
                }
            }
            catch (SocketException e)
            {
                Host.Logger.WriteLine(String.Format("SocketException: {0}", e));
            }
            finally
            {
                mListener.Stop();
            }
        }

        private void Regist(string aServer, IpPor aIpPor)
        {
            if (mServers.ContainsKey(aServer)) return;
            mServers.Add(aServer, aIpPor);

        }   


        #endregion

    }
}
