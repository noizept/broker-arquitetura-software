﻿using System;
using System.IO;

using HttpServer;
using HttpServer.Sessions;

namespace ArchBench.PlugIns.Login
{
    public class PlugInLogin : IArchServerModulePlugIn
    {
        #region IArchServerModulePlugIn Members

        public bool Process( IHttpRequest aRequest, IHttpResponse aResponse, IHttpSession aSession )
        {
            if ( ! aRequest.Uri.AbsolutePath.StartsWith( "/login" ) ) return false;

            foreach ( RequestCookie cookie in aRequest.Cookies )
            {
                Host.Logger.WriteLine( "Cookie: {0} = {1}", cookie.Name, cookie.Value );                
            }

            Host.Logger.WriteLine( String.Format( "Accept request for : {0}", aRequest.Uri.ToString() ) );
            if ( aRequest.Method == Method.Get )
            {
                StreamWriter writer = new StreamWriter(aResponse.Body);
                writer.Write( Resource.login );
                writer.Flush();
                aResponse.Send();
            }
            else if (aRequest.Method == Method.Post)
            {

                foreach ( HttpInputItem item in aRequest.Form )
                {
                    Host.Logger.WriteLine( "==> [{0}] := {1}", item.Name, item.Value );
                    
                }

                if ( aRequest.Form.Contains( "Username" ) )
                {
                    aSession[ "Username" ] = aRequest.Form[ "Username" ].Value;
                }

                if (aRequest.Form.Contains("Username"))
                {
                    Host.Logger.WriteLine(String.Format("User {0} logged on.", aRequest.Form["Username"].Value));
                    StreamWriter writer = new StreamWriter(aResponse.Body);
                    writer.WriteLine("<p>TESTETETETE_!_!_</p>");
                    writer.Flush();
                    aResponse.Send();
                }
                else
                    Host.Logger.WriteLine("Error: invalid login data.");
            }

            return true;
        }

        #endregion

        #region IArchServerPlugIn Members

        public string Name
        {
            get { return "ArchServer Login Plugin"; }
        }

        public string Description
        {
            get { return "Process /user/login/ requests"; }
        }

        public string Author
        {
            get { return "Leonel Nobrega"; }
        }

        public string Version
        {
            get { return "1.0"; }
        }

        public IArchServerPlugInHost Host
        {
            get; set;
        }

        public void Initialize()
        {
        }

        public void Dispose()
        {
        }

        #endregion
    }
}
