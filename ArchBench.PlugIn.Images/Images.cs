﻿using System.IO;
using HttpServer;
using HttpServer.Sessions;
using System.Drawing;
using System.Drawing.Imaging;
using ArchBench.PlugIn.Images.Properties;
using System.IO;
using HttpServer;

namespace ArchBench.PlugIn.Images
{
    public class Images : IArchServerModulePlugIn
	{
		public string Name
		{
			get { return "Images PlugIn"; }
		}

		public string Description
		{
			get { return "Images"; }
		}

		public string Author
		{
			get { return "Lino Oliveira"; }
		}

		public string Version
		{
			get { return "1.0"; }
		}


        public IArchServerPlugInHost Host { get; set; }

		public void Initialize()
		{
			Host.Logger.WriteLine("PlugIn Images initialized...");
		}

        public void Dispose()
        {
            throw new System.NotImplementedException();
        }

        public bool Process(IHttpRequest aRequest, IHttpResponse aResponse, IHttpSession aSession)
		{
			Image imageIn;
			string extension = aRequest.Uri.AbsolutePath.Substring(aRequest.Uri.AbsolutePath.LastIndexOf('.') + 1);

			switch (aRequest.Uri.AbsolutePath)
			{
				case "/imgs/img1.jpg":
					imageIn = Resources.image1;
					extension = "jpeg";
					break;
				default:
					return false;
			}

			MemoryStream ms = new MemoryStream();
			imageIn.Save(ms, imageIn.RawFormat);

			aResponse.AddHeader("Content-Type", "image/" + extension);
			aResponse.AddHeader("Content-Length", ms.ToArray().Length.ToString());

			aResponse.Body = ms;

			return true;
		}
	}
}
