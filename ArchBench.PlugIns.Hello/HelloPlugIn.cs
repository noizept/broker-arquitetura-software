﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Runtime.Versioning;
using System.Text;
using System.Threading.Tasks;
using ArchBench.PlugIns.Hello.Properties;
using HttpServer;
using System.IO;
using System.Drawing;





namespace ArchBench.PlugIns.Hello
{
    public class HelloPlugIn : IArchServerModulePlugIn
    {
        public bool Process( IHttpRequest aRequest, IHttpResponse aResponse, HttpServer.Sessions.IHttpSession aSession)
        {
            if (aRequest.Uri.AbsolutePath.StartsWith("/image"))
            {

                Image imagIn = Resources._1;


                MemoryStream ms = new MemoryStream();
                imagIn.Save(ms, imagIn.RawFormat);
                
                
                aResponse.Body = new MemoryStream();
                aResponse.Body = ms;
                aResponse.ContentType = "image/jpeg";
                aResponse.Send();
            }

            if (aRequest.Uri.AbsolutePath.StartsWith("/hello"))
            {
                Host.Logger.WriteLine("Accept request for : {0}", aRequest.Uri.ToString());

                StreamWriter writer = new StreamWriter(aResponse.Body);
                writer.WriteLine("Hello dude..dude!");

                foreach (RequestCookie cookie in aRequest.Cookies)
                {
                    Host.Logger.WriteLine("Cookie: {0} = {1}", cookie.Name, cookie.Value);
                }
               
                writer.WriteLine(aResponse.Cookies);

                writer.Flush();
                

                aResponse.Send();

                return true;
            }
            return false;
        }

        public string Name
        {
            get { return "Hello PlugIn"; }
        }

        public string Description
        {
            get { return "Say Hello"; }
        }

        public string Author
        {
            get { return "Leonel Nobrega"; }
        }

        public string Version
        {
            get { return "0.1"; }
        }

        public IArchServerPlugInHost Host
        {
            get;
            set;
        }

        public void Initialize()
        {
        }

        public void Dispose()
        {
        }
    }
}
