﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using ArchBench;
using HttpServer;
using HttpServer.Sessions;


namespace ArchBench.PlugIn.Login
{
    public class Login : IArchServerModulePlugIn
    {
        public string Name
        {
            get { return "Login PlugIn"; }
        }

        public string Description
        {
            get { return "Login"; }
        }

        public string Author
        {
            get { return "Luis Martins"; }
        }

        public string Version
        {
            get { return "1.0"; }
        }

        public IArchServerPlugInHost Host
        {
            get;
            set;
        }


        public void Initialize()
        {
            Host.Logger.WriteLine(GetTimestamp()+": PlugIn Login initialized..." );
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }


        public bool Process(HttpServer.IHttpRequest aRequest, HttpServer.IHttpResponse aResponse, HttpServer.Sessions.IHttpSession aSession)
        {
            //aResponse = GenerateSessionCookie(aRequest, aResponse);
            if (aRequest.Uri.AbsolutePath == "/user/img1.jpg")
            {
                LoadImage(aResponse, Resource.img1, "jpg");
            }
            if (aRequest.Uri.AbsolutePath == "/user/img2.jpg")
            {
                LoadImage(aResponse, Resource.img2, "jpg");
            }
            if (aRequest.Uri.AbsolutePath == "/user/login" || aRequest.Uri.AbsolutePath == "/user/login/")
            {
                if ( aRequest.Method == Method.Get )
                {
                    ProcessGet(aRequest, aResponse, aSession);
                }
                else if ( aRequest.Method == Method.Post )
                {
                    aSession = ProcessPost(aRequest, aResponse, aSession);
                }
                return true;
            }
            return false;
        }

        public static String GetTimestamp()
        {
            return DateTime.Now.ToString("HH:mm:ss yyyy:MM:dd");
        }

        public string GetCookieValue(HttpServer.IHttpRequest aRequest, string cookie_name)
        {
            foreach (RequestCookie cookie in aRequest.Cookies)
            {
                if (cookie.Name == cookie_name)
                {
                    return cookie.Value;
                }
            }
            return "";
        }

        public IHttpResponse GenerateSessionCookie(IHttpRequest aRequest, IHttpResponse aResponse)
        {
            string sess_cookie = GetCookieValue(aRequest, "idsession");
            if (String.IsNullOrEmpty(sess_cookie))
            {
                Guid g = Guid.NewGuid();
                string GuidString = Convert.ToBase64String(g.ToByteArray());
                ResponseCookie cookie = new ResponseCookie("idsession", GuidString, DateTime.Now.AddMonths(1));
                aResponse.Cookies.Add(cookie);
            }
            return aResponse;
        }

        public void LoadImage(IHttpResponse aResponse, Bitmap image, string imageType)
        {
            Bitmap a = Resource.img1;
            aResponse.AddHeader("Content-Type", "image/"+imageType);
            StreamWriter writer = new StreamWriter(aResponse.Body);
            ImageConverter converter = new ImageConverter();
            Byte[] img = (byte[])converter.ConvertTo(image, typeof(byte[]));
            writer.BaseStream.Write(img, 0, img.Length);
            aResponse.AddHeader("Content-Length", "" + img.Length);
            writer.Flush();
            aResponse.Send();
        }

        public void DisplayCookies(IHttpRequest aRequest)
        {
            //Host.Logger.WriteLine(GetTimestamp() + ": Cookies: '{0}'", aRequest.Cookies.Count);
            foreach (RequestCookie cookie in aRequest.Cookies)
            {
                Host.Logger.WriteLine(GetTimestamp()+": Cookie({0}) = {1}", cookie.Name, cookie.Value);
            }
        }

        public void ProcessGet(IHttpRequest aRequest, IHttpResponse aResponse, IHttpSession aSession)
        {
            var writer = new StreamWriter(aResponse.Body);
            Host.Logger.WriteLine(GetTimestamp()+": Server IP {0}:{1}", aRequest.Uri.Host, aRequest.Uri.Port);
            if (aSession["Username"] != null)
            {
                DisplayCookies(aRequest);
                Host.Logger.WriteLine(GetTimestamp()+": User {0} logged on. ReqCookies: {1} RespCookies: {2}", aSession["Username"], aRequest.Cookies.Count, aResponse.Cookies.Count);
                writer.WriteLine("User {0} logged on.", aSession["Username"]);
                //writer.WriteLine("<br /><br /><a href=\"/user/logout/\">Logout</a><br /><img src='/user/img2.jpg'></img>");
                writer.WriteLine(String.Format("<br /><br /><a href=\"http://{0}/user/logout\">Logout</a><br /><img src=\"http://{0}/user/img2.jpg\"></img>", aRequest.Uri.Host + ":" + aRequest.Uri.Port));
                writer.Flush();
            }
            else
            {
                //Test absolute paths
                String form = Resource.Login.Replace("/user/login", String.Format("http://{0}:{1}/user/login", aRequest.Uri.Host, +aRequest.Uri.Port));
                form = form.Replace("/user/img1.jpg", String.Format("http://{0}:{1}/user/img1.jpg", aRequest.Uri.Host, +aRequest.Uri.Port));
                writer.Write(form);
                //Load login form
                //writer.Write(Resource.Login);
                writer.Flush();
            }
            aResponse.Send();
        }

        public IHttpSession ProcessPost(IHttpRequest aRequest, IHttpResponse aResponse, IHttpSession aSession)
        {
            Host.Logger.WriteLine(GetTimestamp()+ ": User '{0}' login...", aRequest.Form["Username"].Value);
            aSession["Username"] = aRequest.Form["Username"].Value;
            //Host.Logger.WriteLine("HOST {0}:{1}", aRequest.Uri.Host, aRequest.Uri.Port);
            var writer = new StreamWriter(aResponse.Body);
            writer.WriteLine("User {0} logged on.", aSession["Username"]);
            //writer.WriteLine("<br /><br /><a href=\"/user/logout\">Logout</a><br /><img src='/user/img2.jpg'></img>");
            writer.WriteLine(String.Format("<br /><br /><a href=\"http://{0}/user/logout\">Logout</a><br /><img src=\"http://{0}/user/img2.jpg\"></img>", aRequest.Uri.Host + ":" + aRequest.Uri.Port));
            writer.Flush();
            return aSession;
        }
    }
}
